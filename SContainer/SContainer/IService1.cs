﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;


namespace SContainer.BD
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
       


        // TODO: Add your service operations here
      
        //Listado Containers
        [OperationContract]
        List<Container> ListadoContainer();

        //[WebGet(ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "ListadoContainer")]
     
        //Guardar un registro

        [OperationContract]
        void crearregistro(Container conta);


        [OperationContract]
        void editarregistro(Container conta);

        [OperationContract]
        Container getcontainer(int id);

    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {

    }
}
