﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using SContainer.BD;
using System.Data.Entity;

namespace SContainer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public List<Container> ListadoContainer()
        {
            //Koala3Entities1 bdusuario = new Koala3Entities1();
            // var entity = (from p in bdusuario.Containers where p.equipmentNbr == "22" select p).FirstOrDefault();
            // return entity;
            // var lista = (from p in bd.Containers select p).ToList();
            // return bdusuario.Containers.ToList();

           // List<Container> lista = new List<Container>();

            using (Koala3Entities1 bd = new Koala3Entities1())
            {
                bd.Configuration.LazyLoadingEnabled = false;
                bd.Configuration.ProxyCreationEnabled = false;
                var lista = (from p in bd.Containers select p).ToList();
                return lista;
            }
        
        }



        public Container getcontainer(int id)
        {
            using (Koala3Entities1 bd = new Koala3Entities1())
            {
                bd.Configuration.LazyLoadingEnabled = false;
                bd.Configuration.ProxyCreationEnabled = false;
                var linea = (from a in bd.Containers where a.gkey == id select a).First();
  
                return linea;
            }
        }


        public void crearregistro(Container conta)
        {
        
            using (Koala3Entities1 bd = new Koala3Entities1())
            {
                bd.Containers.Add(conta);
                bd.SaveChanges();
            }
        }


        public void editarregistro(Container conta)
        {
            using (Koala3Entities1 bd = new Koala3Entities1())
            {
                bd.Configuration.LazyLoadingEnabled = false;
                bd.Configuration.ProxyCreationEnabled = false;
                var linea = (from a in bd.Containers where a.gkey == conta.gkey select a).FirstOrDefault();

                if (linea != null)
                {

                    linea.equipmentNbr = conta.equipmentNbr;
                    linea.typeIso = conta.typeIso;
                    linea.own = conta.own;
                    linea.typeLength = conta.typeLength;
                    linea.tareWt = conta.tareWt;
                    linea.safeWt = conta.safeWt;
                    bd.SaveChanges();
                }

                
               
            }

        }


 

    }
}
