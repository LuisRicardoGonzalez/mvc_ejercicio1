﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LUISMVC.DB;

namespace LUISMVC.Controllers
{
    public class VesselVisitsController : Controller
    {
        private Koala3Entities db = new Koala3Entities();

        // GET: VesselVisits
        public ActionResult Index()
        {
            return View(db.VesselVisits.ToList());
        }

        // GET: VesselVisits/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VesselVisit vesselVisit = db.VesselVisits.Find(id);
            if (vesselVisit == null)
            {
                return HttpNotFound();
            }
            return View(vesselVisit);
        }

        // GET: VesselVisits/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VesselVisits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,visit,line,vesselName,phase,ata,atd,eta,etd,serv")] VesselVisit vesselVisit)
        {
            if (ModelState.IsValid)
            {
                db.VesselVisits.Add(vesselVisit);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vesselVisit);
        }

        // GET: VesselVisits/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VesselVisit vesselVisit = db.VesselVisits.Find(id);
            if (vesselVisit == null)
            {
                return HttpNotFound();
            }
            return View(vesselVisit);
        }

        // POST: VesselVisits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,visit,line,vesselName,phase,ata,atd,eta,etd,serv")] VesselVisit vesselVisit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vesselVisit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vesselVisit);
        }

        // GET: VesselVisits/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VesselVisit vesselVisit = db.VesselVisits.Find(id);
            if (vesselVisit == null)
            {
                return HttpNotFound();
            }
            return View(vesselVisit);
        }

        // POST: VesselVisits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VesselVisit vesselVisit = db.VesselVisits.Find(id);
            db.VesselVisits.Remove(vesselVisit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
