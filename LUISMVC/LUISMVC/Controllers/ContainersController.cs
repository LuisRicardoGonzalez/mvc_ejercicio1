﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LUISMVC.DB;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;
using LUISMVC.Models.Containers;


namespace LUISMVC.Controllers
{
    public class ContainersController : Controller
    {
        private Koala3Entities db = new Koala3Entities();


        //////////////////////////////////////////////////// GET: Containers
        public ActionResult Index()
        {
            var srvc = new wscontainer.Service1Client();
           return View(srvc.ListadoContainer().ToList());

            // return View(db.Containers.ToList());
        }



        /////////////////////////////////////////////////// GET: Containers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           // Container container = db.Containers.Find(id);
            var srvc = new wscontainer.Service1Client();

            if (srvc == null)
            {
                return HttpNotFound();
            }
            return View(srvc);
        }

        ////////////////////////////////// GET: Containers/Create
        [HttpGet] 
        public ActionResult Create()
        {
            return View(new CreateContainerViewModel());
        }

        [HttpPost]
        public ActionResult CreateContainer(CreateContainerViewModel container)
        {
       
                using (wscontainer.Service1Client db = new wscontainer.Service1Client())
                {


                    db.crearregistro(AutoMapper.Mapper.Map<CreateContainerViewModel,wscontainer.Container>(container));

                    return RedirectToAction("Index");
                }
            
        }


        /////////////////////////////////////////////////////////////////// GET: Containers/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (wscontainer.Service1Client db = new wscontainer.Service1Client())
            {
                

                var cont = db.getcontainer(id);

                var container = AutoMapper.Mapper.Map<wscontainer.Container, CreateContainerViewModel>(cont);

                return View(container);

            }

        }

        // POST: Containers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CreateContainerViewModel container)
        {

            using (wscontainer.Service1Client db = new wscontainer.Service1Client())
            {

                db.editarregistro(AutoMapper.Mapper.Map<CreateContainerViewModel, wscontainer.Container>(container));
      

                return RedirectToAction("Index");
            }

                
        }






        // GET: Containers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Container container = db.Containers.Find(id);
            if (container == null)
            {
                return HttpNotFound();
            }
            return View(container);
        }

        // POST: Containers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Container container = db.Containers.Find(id);
            db.Containers.Remove(container);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
