﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LUISMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Vessel()
        {
      
            return RedirectToAction("VesselVisit/Index");
        
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult VesselVisit()
        {
            ViewBag.Message = "VesselVisit table";

            return View();
        }




    }
}