﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LUISMVC.DB;

namespace LUISMVC.Controllers
{
    public class UnitsController : Controller
    {
        private Koala3Entities db = new Koala3Entities();

        // GET: Units
        public ActionResult Index()
        {
            var units = db.Units.Include(u => u.Container).Include(u => u.VesselVisit).Include(u => u.VesselVisit1);
            return View(units.ToList());
        }

        // GET: Units/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unit unit = db.Units.Find(id);
            if (unit == null)
            {
                return HttpNotFound();
            }
            return View(unit);
        }

        // GET: Units/Create
        public ActionResult Create()
        {
            ViewBag.unitNbr = new SelectList(db.Containers, "gkey", "equipmentNbr");
            ViewBag.ibActualVisit = new SelectList(db.VesselVisits, "id", "visit");
            ViewBag.obActualVisit = new SelectList(db.VesselVisits, "id", "visit");
            return View();
        }

        // POST: Units/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "gkey,unitNbr,lineOp,ibActualVisit,obActualVisit,category,vStat,tState,frghtKind")] Unit unit)
        {
            if (ModelState.IsValid)
            {
                db.Units.Add(unit);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.unitNbr = new SelectList(db.Containers, "gkey", "equipmentNbr", unit.unitNbr);
            ViewBag.ibActualVisit = new SelectList(db.VesselVisits, "id", "visit", unit.ibActualVisit);
            ViewBag.obActualVisit = new SelectList(db.VesselVisits, "id", "visit", unit.obActualVisit);
            return View(unit);
        }

        // GET: Units/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unit unit = db.Units.Find(id);
            if (unit == null)
            {
                return HttpNotFound();
            }
            ViewBag.unitNbr = new SelectList(db.Containers, "gkey", "equipmentNbr", unit.unitNbr);
            ViewBag.ibActualVisit = new SelectList(db.VesselVisits, "id", "visit", unit.ibActualVisit);
            ViewBag.obActualVisit = new SelectList(db.VesselVisits, "id", "visit", unit.obActualVisit);
            return View(unit);
        }

        // POST: Units/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "gkey,unitNbr,lineOp,ibActualVisit,obActualVisit,category,vStat,tState,frghtKind")] Unit unit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(unit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.unitNbr = new SelectList(db.Containers, "gkey", "equipmentNbr", unit.unitNbr);
            ViewBag.ibActualVisit = new SelectList(db.VesselVisits, "id", "visit", unit.ibActualVisit);
            ViewBag.obActualVisit = new SelectList(db.VesselVisits, "id", "visit", unit.obActualVisit);
            return View(unit);
        }

        // GET: Units/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Unit unit = db.Units.Find(id);
            if (unit == null)
            {
                return HttpNotFound();
            }
            return View(unit);
        }

        // POST: Units/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Unit unit = db.Units.Find(id);
            db.Units.Remove(unit);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
