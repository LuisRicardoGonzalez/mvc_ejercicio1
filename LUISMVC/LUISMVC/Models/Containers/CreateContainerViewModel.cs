﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LUISMVC.Models.Containers
{
    public class CreateContainerViewModel
    {
        
        public string Gkey { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Equipo")]
        public string EquipmentNbr { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Tipo Iso")]
        public string TypeIso { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Owner")]
        public string Own { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Type Length")]
        public string TypeLength { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Tare Weight")]
        public int TareWt { get; set; }

        [Required(ErrorMessage = "Campo Obligatorio")]
        [Display(Name = "Safe Weight")]
        public int SafeWt { get; set; }

    }

   

}