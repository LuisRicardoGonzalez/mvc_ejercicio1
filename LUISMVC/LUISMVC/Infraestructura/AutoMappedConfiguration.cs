﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using LUISMVC.Models.Containers;

using LUISMVC.wscontainer;

namespace LUISMVC.Infraestructura
{
    public class AutoMappedConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x => {
                x.CreateMap<CreateContainerViewModel, Container>()
                     .ForMember(z => z.gkey, y => y.MapFrom(a => a.Gkey))
                    .ForMember(z => z.equipmentNbr, y => y.MapFrom(a => a.EquipmentNbr))
                    .ForMember(z => z.typeIso, y => y.MapFrom(a => a.TypeIso))
                    .ForMember(z => z.own, y => y.MapFrom(a => a.Own))
                    .ForMember(z => z.typeLength, y => y.MapFrom(a => a.TypeLength))
                    .ForMember(z => z.tareWt, y => y.MapFrom(a => a.TareWt))
                    .ForMember(z => z.safeWt, y => y.MapFrom(a => a.SafeWt));

                x.CreateMap<Container, CreateContainerViewModel>()
                   .ForMember(z => z.Gkey, y => y.MapFrom(a => a.gkey))
                   .ForMember(z => z.EquipmentNbr, y => y.MapFrom(a => a.equipmentNbr))
                   .ForMember(z => z.TypeIso, y => y.MapFrom(a => a.typeIso))
                   .ForMember(z => z.Own, y => y.MapFrom(a => a.own))
                   .ForMember(z => z.TypeLength, y => y.MapFrom(a => a.typeLength))
                   .ForMember(z => z.TareWt, y => y.MapFrom(a => a.tareWt))
                   .ForMember(z => z.SafeWt, y => y.MapFrom(a => a.safeWt));
            });
        }


    }


}